# Establece la imagen base
FROM nginx:alpine

# Copia los archivos de la aplicación web en el contenedor
COPY . /usr/share/nginx/html

# Expone el puerto 80 para que la aplicación sea accesible desde fuera del contenedor
EXPOSE 80

# Inicia el servidor nginx
CMD ["nginx", "-g", "daemon off;"]
